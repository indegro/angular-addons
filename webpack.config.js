var path = require('path');
var webpack = require('webpack');

var config = module.exports = {
  context: __dirname,
  entry: './index.js',
};

config.devtool = 'source-map';

config.output = {
  path: path.join(__dirname, 'webpack-example'),
  filename: 'bundle.js'
};

config.module = {
  loaders: [
    {
      test: /\.jsx?$/,
      exclude: /(node_modules|bower_components)/,
      loaders: ['babel']
    }
  ]
};

config.resolve = {
  extensions: ['', '.js'],
  modulesDirectories: [ 'node_modules', 'bower_components' ],
  alias: {
    'bowerUiMap': 'angular-ui-map/ui-map.js',
    'bowerUiUtils': 'angular-ui-utils/ui-utils.js'
  }
};

config.devServer = {
  contentBase: './',
  noInfo: true, //  --no-info option
  hot: false,
  inline: true
};

// config.plugins = [
//   new webpack.ProvidePlugin({
//     "window.angular": "angular"
//   })
// ];
