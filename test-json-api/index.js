var jsonServer = require('json-server')

var server = jsonServer.create()
server.use(jsonServer.defaults())

var router = jsonServer.router('db.json')
router.render = function (req, res) {
	if (req.route.path === '/:id') {
		res.jsonp({
	   post: res.locals.data
	  })
	} else {
		res.jsonp({
		 posts: res.locals.data
		})
	}
}

server.use(router)
server.listen(3000)
