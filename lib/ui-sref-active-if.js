// from https://github.com/angular-ui/ui-router/issues/1431#issuecomment-121929944
export default ($state) => {
  return {
    restrict: 'A',
    controller: ($scope, $element, $attrs) => {
			var state = $attrs.uiSrefActiveIf;

			function update() {
				if ($state.current.name.indexOf(state) === 0 || $state.is(state)) {
					$element.addClass('active');
				} else {
					$element.removeClass('active');
				}
			}

			$scope.$on('$stateChangeSuccess', update);
			update();
    }
  };
};
