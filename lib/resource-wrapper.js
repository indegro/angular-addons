let customResource = require('./resource');

export let name = 'Resource';
export let service = ($q, $resource, growl, $state) => {
  return (url, modelName, options) => {
    return customResource($q, $resource, growl, $state, url, modelName, options);
  };
};
