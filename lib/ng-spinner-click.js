// https://github.com/angular/angular.js/blob/master/src/ng/directive/ngEventDirs.js#L3
export let name = 'ngSpinnerClick';
export let directive = ($parse) => {
  return {
    restrict: 'A',
    compile: function($element, attr) {
      var fn = $parse(attr['ngSpinnerClick'], null, true);

      return function ngEventHandler(scope, element) {
        var loading = false;

        element.on('click', function(event) {
          if (loading) {
            return true;
          }

          loading = true;

          var callback = function() {
            return fn(scope, { $event: event });
          };

          var html = element.html();

          element.html('<i class="fa fa-circle-o-notch fa-spin"></i>');

          scope.$apply(callback).then(function() {
            element.html(html);
            loading = false;
          }, function(err) {
            element.html(html);
            loading = false;
          });
        });
      };
    }
  };
};
