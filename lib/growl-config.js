module.exports = function(growlProvider) {
  growlProvider.onlyUniqueMessages(false);
  growlProvider.globalTimeToLive({
    success: 3000,
    error: 3000,
    warning: 3000,
    info: 60000 // for user password
  });
};
