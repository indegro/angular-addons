import * as serverErrors from './formly-errors-summary-service';

let form = (fields) => {
	let result = {};

	result.addServerErrors = (err) => {
		serverErrors.addServerErrors(result, err);
	};

	serverErrors.traverse(fields, (field) => {
		if (field.key) {
			field.validators = field.validators || {};
			field.validators.server = {
				expression: ($viewValue, $modelValue, scope) => {
					if (scope.fc) {
						scope.fc.$setValidity('server', true);
						scope.fc.$serverErrors = [];
					}

					if (result.$serverErrors) {
						delete result.$serverErrors[scope.options.key];
					}

					return true;
				}
			};
		}
	});

	result.fields = fields;
	return result;
};

export let name = 'FormlyAddons';
export let service = () => {
	return {
		form: form
	};
};
