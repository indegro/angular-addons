import _ from 'lodash';

export let textInput = (key, label, params, attrs = {}) => {
  var templateOptions = {
    ...params,
    label: label,
    placeholder: label
  };

  return {
    ...attrs,
    type: 'input',
    key: key,
    templateOptions: templateOptions
  };
};

export let select = (key, label, options, attrs = {}) => {
  var selectOptions;

  if (typeof options === 'string') {
    selectOptions = [];
    _.each(window.CURRENT_USER.enums[options], (val, key) => selectOptions.push({value: key, name: val }) );
  } else {
    selectOptions = options;
  }

  return {
    ...attrs,
    type: 'select',
    key: key,
    templateOptions: {
      label: label,
      options: selectOptions
    }
  };
};

export let checkbox = (key, label, params = {}, attrs = {}) => {
  return {
    ...attrs,
    type: 'checkbox',
    key: key,
    templateOptions: {
      ...params,
      label: label
    }
  };
};

export let utils = {
  objectToSelectOptions: (object) => {
    return _.map(object, (label, value) => {
      return {
        name: label,
        value: value
      };
    });
  }
};
