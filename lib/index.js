let libName = 'angular-addons';

require('imports?angular=angular!ng-resource')(window, angular);
require('imports?angular=angular!angular-growl-v2/build/angular-growl.js');

var app = angular.module(libName, [
	'ngResource',
  'angular-growl',
  require('angular-ui-router'),
	require('angular-formly'),
	require('angular-formly-templates-bootstrap')
]);

var dep = null;

dep = require('./formly-errors-summary-directive');
app.directive(dep.name, dep.directive);

dep = require('./ng-spinner-click');
app.directive(dep.name, dep.directive);

dep = require('./formly-addons');
app.service(dep.name, dep.service);

dep = require('./resource-wrapper');
app.service(dep.name, dep.service);

dep = require('./confirm');
app.service('$confirm', dep.default);

dep = require('./ui-sref-active-if');
app.directive('uiSrefActiveIf', dep.default);

app.config(require('./resource-config'));
app.config(require('./growl-config'));

export default libName;
