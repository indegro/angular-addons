export default ($q) => {

  return (cb, message = 'Are you sure?') => {
    if (confirm(message)) {
      return cb();
    } else {
      var deferred = $q.defer();
      deferred.resolve(false);
      return deferred.promise;
    }
  };

};
