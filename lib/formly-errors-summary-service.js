import _ from 'lodash';

export let traverse = (fields, predicate) => {
  _.each(fields, (field) => {
		if (field.fieldGroup) {
			_.each(field.fieldGroup, (field) => {
				predicate(field);
			});
		} else {
			predicate(field);
		}
	});
};

export let addServerErrors = (form, errors, options = {}) => {
  form.$serverErrors = {};

	_.each(errors, (errorMessage, errorKey) => {
		traverse(form.fields, (field) => {
			if (errorKey === field.key) {
        form.$serverErrors[field.key] = {
					label: field.templateOptions.label,
					value: errorMessage.join(', ')
				};

        if (!options.disableInvalid) {
          field.formControl.$setValidity('server', false);
        }

        field.formControl.$setPristine(true);
			}
		});
	});
};

export let clearServerErrors = (form) => {
  form.$serverErrors = {};

	traverse(form.fields, (field) => {
		if (field.formControl) {
			field.formControl.$setValidity('server', true);
		}
	});
};
