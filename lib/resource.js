import moment from 'moment';
import pluralize from 'pluralize';
import _ from 'lodash';

var lazyMoment = function(model, field) {
  Object.defineProperty(model, '_' + field, {
    get: function() {
      if (!this['__' + field]) {
        this['__' + field] = moment(this[field]);
      }
      return this['__' + field];
    }
  });
};

var transformRequest = function(modelName) {
  return function(data) {
    var result = {};
    var actualData = {};

    for (var i in data) {
      if (data.hasOwnProperty(i) && i[0] != '_' && i[0] != '$') {
        actualData[i] = data[i];
      }
    }

    result[modelName] = actualData;
    return JSON.stringify(result);
  };
};

var transformResponse = function(model, modelName, $q, growl) {
  return (response) => {
    var data = JSON.parse(response);

    if (data.flash && data.flash.error) {
      growl.error(data.flash.error);
    }

    if (data.errors) {
      return $q.reject(data.errors);
    }

    if (data.flash && data.flash.error) {
      return $q.reject(data);
    }

    if (data[modelName]) {
      return data[modelName];
    }

    return data;
  };
};

var patchedMethod = function(method, model, modelName, $q, growl, options = {}) {
  return {
    ...options,
    method: method,
    transformRequest: transformRequest(modelName),
    transformResponse: transformResponse(model, modelName, $q, growl),
    interceptor: {
      response: (response) => {
        return response.data;
      }
    }
  };
};

var customResource = function($q, $resource, growl, $state, url, modelName, options) {
  options = options || {};

  var params = { id: '@id', access: () => window.CURRENT_USER && window.CURRENT_USER.access };
  var model = $resource(url, params, {
    'get': patchedMethod('GET', model, modelName, $q, growl),
    'query': patchedMethod('GET', model, modelName, $q, growl),
    'update': patchedMethod('PUT', model, modelName, $q, growl),
    'create': patchedMethod('POST', model, modelName, $q, growl),
    'remove': patchedMethod('DELETE', model, modelName, $q, growl)
  });

  model.prototype.$upsert = function(afterCreateState) {
    var id = this.id;
    var that = this;

    if (id) {
      return this.$update({ id: id }).then(function(response) {
        growl.success(options.updateSuccess || 'Успешно');
        return new model(response);
      }, function(err) {
        growl.error(options.updateError || 'Ошибка');
        return $q.reject(err);
      });
    } else {
      return this.$create().then(function(response) {
        growl.success(options.updateSuccess || 'Успешно');

        if (afterCreateState) {
          $state.go(afterCreateState, { id: this.id || this[modelName] && this[modelName].id  });
        }

        return new model(response);
      }.bind(this), function(err) {
        growl.error(options.updateError || 'Ошибка');
        return $q.reject(err);
      });
    }
  };

  var pluralized = pluralize(modelName);

  var superQuery = model.query;
  model.query = (params) => {
    return superQuery.call(model, params).$promise.then((result) => {
      if (result[pluralized]) {
        result[pluralized] = _.map(result[pluralized], (item) => new model(item));
      }
      return result;
    });
  };

  var patchMethod = (methodName) => {
    var superMethod = model.prototype[methodName];
    model.prototype[methodName] = ((params) => {
      return superMethod.call(model, params).then((result) => new model(result));
    }).bind(model);
  };

  // patchMethod('$update');
  // patchMethod('$create');
  // patchMethod('$remove');

  model.getOrNew = (params) => {
    let result = null;

    if (params.id) {
      result = model.get(params).$promise;
    } else {
      result = $q.resolve({});
    }

    return result.then((data) => new model(data));
  };

  lazyMoment(model.prototype, 'updated_at');
  lazyMoment(model.prototype, 'created_at');
  lazyMoment(model.prototype, 'date');

  lazyMoment(model.prototype, 'begin');
  lazyMoment(model.prototype, 'end');

  return model;
};

module.exports = customResource;
