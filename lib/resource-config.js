module.exports = function($resourceProvider) {
  $resourceProvider.defaults.actions = {
    create: {method: 'POST'},
    save:   {method: 'POST'},
    update: {method: 'PUT'},
    update_put: {method: 'PUT'},
    get:    {method: 'GET'},
    query:  { method: 'GET', isArray: true },
    remove: {method: 'DELETE'},
    delete: {method: 'DELETE'}
  };

  $resourceProvider.defaults.stripTrailingSlashes = false;
};
