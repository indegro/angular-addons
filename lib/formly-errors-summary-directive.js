export let name = 'formlyErrorSummary';
export let directive = () => {

  return {
    scope: {},
    bindToController: {
      form: '='
    },
    template: require('!!ng-cache!./formly-errors-summary.html'),
    controllerAs: 'vm',
    controller: function($scope) {
      var vm = this;
    }
  };

};
