# angular-addons

Набор angular-библиотек для:

- Всплывающих уведомлений
- [Генерации форм](docs/forms.md)
- Генерации ajax запросов для работы с ресурсами

## Install

Смерджить в `package.json`:

```json
{
  "dependencies": {
    "angular-addons": "git+ssh://git@bitbucket.org/indegro/angular-addons.git#bc65505cbdf5b85361b39ad24c848a277c87825e"
  },
}
```

Установить зависимость:

```
$ npm install
```

Добавить в исключение в `webpack`:

```javascript
{
  test: /\.jsx?$/,
  exclude: /(node_modules|bower_components)(?!\/angular-addons)/,
  loader: 'babel'
}
```

Подключить в `angular`:

```javascript
var app = angular.module('app', [
  require('angular-addons')
];
```

## Запуск примера

```
$ npm install
$ npm run start
```
