## html

```html
<formly-form model="post" fields="formData.fields">
	<button type="submit" class="btn btn-default" ng-spinner-click="submit($event, 1)">
		Submit
	</button>
</formly-form>

<formly-error-summary form="formData"></formly-error-summary>
```

## resource.js

```js
let url = 'http://jsonplaceholder.typicode.com/posts/:id';

export let name = 'Post';
export let service = (Resource, $resource, growl, $state) => {
	return Resource($resource, growl, $state, url, 'post');
};
```

## form.js

```
export default () => {
	let fields = [
		{
			key: 'userId',
			type: 'input',
			templateOptions: {
				label: 'ФИО'
			}
		}
	];

	return fields;
};
```

## controller.js

```js
export let controller = ($scope, FormlyAddons, Post) => {
	Post.get({ id: 1 }).$promise.then((result) => {
		$scope.post = result;
		$scope.formData = FormlyAddons.form(require('./form')());
	});

	$scope.submit = function($event) {
		return $scope.post.$update().then((result) => {

		}, (err) => {
			$scope.formData.addServerErrors(err);
		});
	};
};
```
