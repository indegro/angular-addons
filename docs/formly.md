## config?

```
app.run(function(formlyConfig, formlyValidationMessages) {
  formlyConfig.extras.errorExistsAndShouldBeVisibleExpression = 'fc.$touched || form.$submitted';
  formlyValidationMessages.addStringMessage('required', 'This field is required');
});
```

## init

```js
import * as helpers from 'angular-addons/lib/formly-helpers';
$scope.formData = FormlyAddons.form(require('./form')($scope));
```

## divs

```js
{
  className: 'kow kow-margin-top',
  fieldGroup: [
    helpers.select('transfer_type', 'Способ передачи курьеру', 'Transfer.transfer_types', { model: attr, className: 'kol-md-2' })
  ]
}
```

## html

```js
{
  template: '<div></div>'
}
```

## text input

```js
{
  key: 'userId',
  type: 'input',
  templateOptions: {
    label: 'ФИО'
  }
}
```

## textarea

```js
{
  type: 'textarea',
  key: 'description',
  templateOptions: {
    label: 'Описание'
  }
}
```

## helper: text input

```js
helpers.textInput('phone', 'Телефон', { required: true }, { })
```

## helper: checkbox

```js
helpers.checkbox('is_phone', 'Телефон', { required: true }, { })
```

## helper: select

```js
var options = _.map($scope.categories, (category) => {
  return {
    name: category.name,
    value: category.id
  };
});
helpers.select('category_id', 'Категория', options, { })
```
