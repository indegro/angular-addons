```js
require('angucomplete-alt');
angular.app('app', ['angucomplete-alt']);
```

```html
<angucomplete-alt
					placeholder="{{ data.placeholder }}"
					pause="200"
					selected-object="data.callback"
					remote-url="/locations?address="
					remote-url-data-field="locations"
					title-field="address"
					description-field="name"
					override-suggestions="true"
					input-changed="data.callback"
					input-class="form-control"/>
```

```js
export let name = 'locationAutocomplete';
export let directive = () => {
  return {
    scope: {
			placeholder: '=',
      callback: '='
		},
    template: require('ng-cache!./location-autocomplete.html'),
		controller: ($scope) => {
			$scope.data = {
				placeholder: $scope.placeholder,
				callback: $scope.callback
			};
		}
  };
};
```

```html
<location-autocomplete placeholder="'Адрес отправления'" callback="data.onAddressFrom"></location-autocomplete>
```

```js
var createFn = (field) => {
	return (place) => {
		if (typeof place === 'object') {
			var location = place.originalObject;
			if (typeof location === 'object') {
				$scope.params[field] = location.address;
			} else if (location) {
				$scope.params[field] = location;
			}
		} else {
			$scope.params[field] = place;
		}
		load();
	};
};

$scope.data = {
	onAddressFrom: createFn('filter_address_from'),
	onAddressTo: createFn('filter_address_to')
};
```
