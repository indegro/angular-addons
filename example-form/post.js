let url = 'http://localhost:3000/posts/:id';

export let name = 'Post';
export let service = (Resource) => {
	return Resource(url, 'post');
};
