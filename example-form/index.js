import angular from 'angular';

var app = angular.module('app', [
	require('./../lib/index')
]);

var dep = null;

dep = require('./posts-show-controller.js');
app.controller(dep.name, dep.controller);
dep = require('./posts-index-controller.js');
app.controller(dep.name, dep.controller);

dep = require('./post');
app.service(dep.name, dep.service);

app.config(($stateProvider, $urlRouterProvider) => {
  $urlRouterProvider.otherwise('/posts');

  $stateProvider
    .state('posts-index', {
      url: '/posts',
      template: require('ng-cache!./posts-index.html'),
      controller: 'PostsIndexController'
    })
    .state('posts-new', {
      url: '/posts/new',
      template: require('ng-cache!./posts-show.html'),
      controller: 'PostsShowController'
    })
    .state('posts-show', {
      url: '/posts/:id',
      template: require('ng-cache!./posts-show.html'),
      controller: 'PostsShowController'
    });
});
