export let name = 'PostsShowController';
export let controller = ($scope, $stateParams, $q, $timeout, FormlyAddons, Post) => {

	Post.getOrNew({ id: $stateParams.id }).then((post) => {
		$scope.post = post;
		$scope.formData = FormlyAddons.form(require('./form')());
	});

	$scope.submit = function($event) {
		return $scope.post.$update().then((result) => {
			$scope.formData.addServerErrors({
				'userId': ['Имя меньше 1 буквы', 'Фамилия с заглавной буквы']
			});
		}, (err) => {
			$scope.formData.addServerErrors({
				'userId': ['Имя меньше 1 буквы', 'Фамилия с заглавной буквы']
			});
		});
	};

};
