export let name = 'PostsIndexController';
export let controller = ($scope, $stateParams, $q, $timeout, FormlyAddons, Post) => {

	var post = new Post({});
	post.$upsert().then((response) => {
		$scope.posts = response.posts;
		console.log('success', response);
	}).catch((err) => {
		console.log('err', err);
	});

};
