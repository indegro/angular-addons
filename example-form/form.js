export default () => {
	let fields = [
		{
			key: 'userId',
			type: 'input',
			templateOptions: {
				label: 'ФИО'
			}
		}
	];

	return fields;
};
